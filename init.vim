"__     ___                    
"\ \   / (_)_ __ ___  _ __ ___ 
" \ \ / /| | '_ ` _ \| '__/ __|
"  \ V / | | | | | | | | | (__ 
"   \_/  |_|_| |_| |_|_|  \___|

call plug#begin()
Plug 'nlknguyen/copy-cut-paste.vim'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'tyru/open-browser.vim'
Plug 'bling/vim-bufferline'
Plug 'jiangmiao/auto-pairs'
Plug 'ryanoasis/vim-devicons'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'reedes/vim-colors-pencil'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-treesitter/playground'
Plug 'vim-airline/vim-airline'
Plug 'airblade/vim-gitgutter'
Plug 'christianchiarulli/nvcode-color-schemes.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
"Requires grip to do previews 
"Use Ctrl-N to select matching words, c to change as in a Visual Block
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'chipsenkbeil/distant.nvim'
call plug#end()

lua <<EOF
require'nvim-treesitter.configs'.setup {
  -- One of "all", "maintained" (parsers with maintainers), or a list of languages
  ensure_installed = "maintained",


  -- List of parsers to ignore installing

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- list of language that will be disabled
    disable = {},

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}


require "nvim-treesitter.configs".setup {
  playground = {
    enable = true,
    disable = {},
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = {
      toggle_query_editor = 'o',
      toggle_hl_groups = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      toggle_language_display = 'I',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    },
  }
}

require('distant').setup {
      -- Applies Chip's personal settings to every machine you connect to
      --
      -- 1. Ensures that distant servers terminate with no connections
      -- 2. Provides navigation bindings for remote directories
      -- 3. Provides keybinding to jump into a remote file's parent directory
      ['*'] = require('distant.settings').chip_default()
}
EOF

"test
" configure nvcode-color-schemes
let g:nvcode_termcolors=256
syntax on
colorscheme pencil " Or whatever colorscheme you make
" you can add these colors to your .vimrc to help customizing
let s:brown = "905532"
let s:aqua =  "3AFFDB"
let s:blue = "689FB6"
let s:darkBlue = "44788E"
let s:purple = "834F79"
let s:lightPurple = "834F79"
let s:red = "AE403F"
let s:beige = "F5C06F"
let s:yellow = "F09F17"
let s:orange = "D4843E"
let s:darkOrange = "F16529"
let s:pink = "CB6F6F"
let s:salmon = "EE6E73"
let s:green = "8FAA54"
let s:lightGreen = "31B53E"
let s:white = "FFFFFF"
let s:rspec_red = 'FE405F'
let s:git_orange = 'F54D27'
set laststatus=2
"let g:NERDTreeNodeDelimiter = "\u00a0"
let g:airline_theme = 'ravenpower'
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#scrollbar#enabled = 0
let g:airline_section_c = '%F'
let g:airline_powerline_fonts = 0
let g:lsp_document_highlight_enabled = 0

"let g:airline#extensions#tabline#enabled = 1
let NERDTreeShowHidden=1
let vim_markdown_preview_github=1
"Change cursor based on current vim mode
let &t_SI = "\<esc>[5 q"
let &t_SR = "\<esc>[5 q"
let &t_EI = "\<esc>[2 q"
let t:is_transparent = 1
highlight clear LspWarningLine
let g:cpp_class_scope_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:go_highlight_functions_calls = 1
let g:go_highlight_structs = 1 
let g:go_highlight_methods = 1
let g:go_highlight_functions = 1
let g:go_highlight_operators = 1
let g:go_highlight_functions_calls=1
let g:go_highlight_build_constraints = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_array_whitespace_error = 1
let g:NERDTreeExtensionHighlightColor = {} " this line is needed to avoid error
let g:NERDTreeExtensionHighlightColor['go'] = s:blue " sets the color of css files to blue
:map <C-f> :NERDTreeToggle<CR>
:map <C-e> :LspHover<CR> 
:map <C-d> :LspDocumentDiagnostics<CR>
:map <C-x> :bd<CR>
:map <C-o> :LspDefinition<CR>
:map <C-s> :FZF .<CR>
:nnoremap <Tab> :bnext<CR>
:nnoremap <S-Tab> :bprevious<CR>
:nnoremap <C-c> :bp\|bd #<CR>
:map <C-t> :sp term://zsh<CR>
nmap nc <Plug>(GitGutterNextHunk)
nmap pc <Plug>(GitGutterPrevHunk)
set backspace=2
set t_Co=256
set  nu  
set bg=dark
set tabstop=4
set shiftwidth=4
"set nocompatible
set enc=utf-8
syntax enable
set clipboard=unnamed
set mouse=a
set noshowmode
set noshowcmd
"set shortmess+=F
"set notermguicolors t_Co=256
" splitting settings
set splitbelow splitright

" if hidden is not set, TextEdit might fail.
set hidden
"set ruler
" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup
"set foldcolumn=0
"set signcolumn=no
" Better display for messages


" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300
highlight GitGutterAdd    guifg=#009900 ctermfg=2
highlight GitGutterChange guifg=#bbbb00 ctermfg=3
highlight GitGutterDelete guifg=#ff2222 ctermfg=1
highlight clear SignColumn

highlight LineNr ctermfg=white 
" don't give |ins-completion-menu| messages.
"set shortmess+=c

" always show signcolumns
"set signcolumn=yes
" :Files to fuzzy search for files
" :Buffer to fuzzy search for buffers
"
vmap <C-c> "+y

"let g:asyncomplete_auto_popup = 0

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction


